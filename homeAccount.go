package main

import (
	"gitee.com/sannychan/home_account/mongo"
	"gitee.com/sannychan/home_account/service"
	"gitee.com/sannychan/go-utils/thttprouter"
	"gitee.com/sannychan/home_account/rest"
	"net/http"
	"flag"
	"gitee.com/sannychan/go-utils/tlog"
)

var (
	address  string
	mongoUrl string
)

func main() {
	tlog.Config(&tlog.Tlog{Level: tlog.DEBUG})
	flag.StringVar(&address, "address", ":8080", "http listen address")
	flag.StringVar(&mongoUrl, "mongo", "", "mongo url")
	flag.Parse()
	tlog.Debug("address:" + address)
	tlog.Info("mongo_url:" + mongoUrl)
	mgo := mongo.New(mongoUrl)
	service.SetMongo(mgo)
	apiv := thttprouter.NewAPI("/account/api")
	rest.Register(apiv)
	tlog.Info("server start at address " + address)
	tlog.Info(http.ListenAndServe(address, apiv.Router))
}
