package mongo

import (
	"gopkg.in/mgo.v2"
)

const DB_NAME = "home_account"

type Mongo struct {
	Session *mgo.Session
	DB      *mgo.Database
}

func New(url string) *Mongo {
	s, err := mgo.Dial(url)
	if err != nil {
		panic(err)
	}
	s.SetMode(mgo.Monotonic, true)
	db := s.DB(DB_NAME)
	return &Mongo{
		Session: s,
		DB:      db,
	}
}

func (mgo *Mongo) GetSession() *mgo.Session {
	return mgo.Session
}

func (mgo *Mongo) Insert(collection string, v interface{}) error {
	c := mgo.DB.C(collection)
	return c.Insert(v)
}
