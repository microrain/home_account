package rest

import (
	"net/http"
	"gitee.com/sannychan/go-utils/thttprouter"
	"gitee.com/sannychan/home_account/model"
	"gitee.com/sannychan/go-utils/tlog"
	"gitee.com/sannychan/home_account/service"
	"time"
)

func addLog(request *http.Request, writer http.ResponseWriter, params thttprouter.Params) {
	l := model.Log{}
	thttprouter.ReadJson(request, writer, &l)
	l.Time = time.Now()
	tlog.Debug("log add param", l)
	err := service.AddLog(l)
	if err != nil {
		tlog.Error("add log err", err)
		thttprouter.Fail(writer, "add log error")
	} else {
		thttprouter.Success(writer, "")
	}
}
