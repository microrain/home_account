package rest

import (
	"gitee.com/sannychan/go-utils/thttprouter"
	"net/http"
	"gitee.com/sannychan/home_account/model"
)

func Register(apiv *thttprouter.API) {
	//base
	apiv.Handle(thttprouter.GET,"/category/list",listCategory)
	apiv.Handle(thttprouter.GET,"/type/list",listTypes)
	apiv.Handle(thttprouter.GET,"/mode/list",listModes)

	//user
	apiv.Handle(thttprouter.GET,"/user",getUser)

	//log
	apiv.Handle(thttprouter.POST,"/log/add",addLog)
}


func listCategory(request *http.Request, writer http.ResponseWriter, params thttprouter.Params) {
	l := model.Categorys
	thttprouter.Success(writer, l)
}

func listTypes(request *http.Request, writer http.ResponseWriter, params thttprouter.Params) {
	l := model.LogTypes
	thttprouter.Success(writer, l)
}

func listModes(request *http.Request, writer http.ResponseWriter, params thttprouter.Params) {
	l := model.LogModes
	thttprouter.Success(writer, l)
}