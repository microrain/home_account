package rest

import (
	"net/http"
	"gitee.com/sannychan/go-utils/thttprouter"
	"gitee.com/sannychan/home_account/service"
)

func getUser(request *http.Request, writer http.ResponseWriter, params thttprouter.Params) {
	openid := request.Form.Get("openid")
	user := service.GetUser(openid)
	thttprouter.Success(writer,user)
}