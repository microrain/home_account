package service

import (
	"gopkg.in/mgo.v2/bson"
	"gitee.com/sannychan/home_account/model"
)

const Collection = "category"

func ListCategory() []model.Category {
	var categorys []model.Category
	mgo.DB.C(Collection).Find(bson.M{}).Sort("order").All(&categorys)
	return categorys
}
