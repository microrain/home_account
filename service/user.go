package service

import (
	"gitee.com/sannychan/home_account/model"
	"gopkg.in/mgo.v2/bson"
)


const UserCollection = "user"

func GetUser(openid string) model.User {
	u := model.User{}
	mgo.DB.C(Collection).Find(bson.M{openid:openid}).One(u)
	return u
}