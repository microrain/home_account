package service

import "gitee.com/sannychan/home_account/model"


const LogCollection = "log"

func AddLog(l model.Log) error {
	err := mgo.DB.C(LogCollection).Insert(l)
	return err
}