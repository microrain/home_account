package model

import "time"

type Category struct {
	Name string `json:"name"`
	Code string `json:"code"`
	Order int64 `json:"order"`
}


type LogType struct {
	Name string `json:"name"`
	Code string `json:"code"`
	Order int64 `json:"order"`
}


type LogMode struct {
	Name string `json:"name"`
	Code string `json:"code"`
	Order int64 `json:"order"`
}

type Log  struct{
	Content string `json:"content"`
	Amount float64 `json:"amount"`
	Category string `json:"category"`
	LogType string `json:"logType"` //账户类型，微信、支付宝等
	LogMode string `json:"logMode"`  //收入支出
	Openid string  `json:"openid"`
	Time time.Time `json:"time"`
}

type Home struct {
	Name string `json:"name"`
	Code string `json:"code"`
	Time time.Time `json:"time"`
}

type User struct {
	Openid string `json:"openid"`
	Name string  `json:"name"`
	Header string `json:"header"`
	Home string `json:"home"`
	Time time.Time `json:"time"`
}