package model

var Categorys = []Category{
	{Name: "吃喝", Code: "eat", Order: 1},
	{Name: "交通", Code: "traffic", Order: 10},
	{Name: "服饰", Code: "dress", Order: 20},
	{Name: "日用品", Code: "daily", Order: 30},
	{Name: "红包", Code: "red", Order: 40},
	{Name: "买菜", Code: "food", Order: 50},
	{Name: "水果", Code: "fruit", Order: 60},
}

var LogTypes = []LogType{
	{Name: "微信", Code: "wechat", Order: 1},
	{Name: "支付宝", Code: "zhifubao", Order: 2},
	{Name: "信用卡", Code: "credit", Order: 3},
	{Name: "储蓄卡", Code: "deposit", Order: 4},
}

var LogModes = []LogMode{
	{Name: "支出", Code: "out", Order: 1},
	{Name: "收入", Code: "in", Order: 2},
}
